# coding=utf8
from tkinter import *
from tkinter import filedialog as fd
import tkinter as tk
from tkinter import ttk
from tkinter import *
from tkinter import messagebox as mb
import datetime

from xml.etree import ElementTree


def openFile():
    fileName = fd.askopenfilename(filetypes=(
        ("Документы Word", "*.doc"), ("Документы Word", "*.docx"),))


class Parser:

    def __init__(self, xml_path="doc/data/p.xml"):
        self.xml_path = xml_path
        self.competences = {}
        self.disciplines = []
        self.profiles = []
        self.parser = ElementTree.XMLParser(encoding='utf-8')
        self.tree = ElementTree.parse("doc/data/p.xml", self.parser)

    def get_competences(self):
        current_key = ''
        tags = [item for item in self.tree.iter(
        ) if item.tag.find("ПланыКомпетенции") != -1]

        for current_tag in tags:
            if 'ШифрКомпетенции' in current_tag.attrib:
                competence_code = current_tag.attrib['ШифрКомпетенции']
                competence_name = current_tag.attrib['Наименование']
                if '.' in competence_code:
                    self.competences[current_key].append(
                        {competence_code: competence_name})
                else:
                    result = f"{competence_code} {competence_name}"
                    self.competences[result] = []
                    current_key = result
        return self.competences

    def get_disciplines(self):
        tags = [item for item in self.tree.iter(
        ) if item.tag.find("ПланыСтроки") != -1]
        for tag in tags:
            discipline_name = tag.attrib['Дисциплина']
            discipline_code = tag.attrib['ДисциплинаКод']
            self.disciplines.append(f"{discipline_code} {discipline_name}")
        return self.disciplines

    def get_profiles(self):
        tags = [item for item in self.tree.iter(
        ) if item.tag.find("псСтандарты") != -1]
        for tag in tags:
            profile_name = tag.attrib['НаименованиеВидаПрофДеятельности']
            self.profiles.append(profile_name)
        return self.profiles


#Окно с титульным листом
def openTitle():
    def saver():
        create = Button(titleEdit, text="Сохранить", command=saver).grid(
            row=13, column=1, sticky="n")
        decanDate = []
        decanDate.append(dataAllList.get())
        decanDate.append(dataAllMonthList.get())
        decanDate.append(dataAllYearList.get())
        subject = subjectsList.get()
        profile = profileList.get()
        protocolNum = protocolNumMessage.get()
        protocolDate = []
        protocolDate.append(protocolDataList.get())
        protocolDate.append(protocolDataMonthList.get())
        protocolDate.append(protocolDataMonthYearList.get())
        nowYear = int((str(now.year))[2:4])
        print("Дата декан", decanDate, "Наименование дисциплины", subject, "Направленность подготовки",
              profile, "Номер протокола",  protocolNum, "Дата протокола", protocolDate, "Текущий год", nowYear)

    titleEdit = Toplevel(menu)
    titleEdit.title('ТИТУЛЬНЫЙ ЛИСТ')
    titleEdit.geometry('700x400')
    titleEdit.resizable(width=False, height=False)

    frame = Frame(titleEdit)

    months = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь',
              'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь']
    days = [i for i in range(1, 32)]
    years = [i for i in range(1, 100)]

    dataAllLabel = Label(titleEdit, text="Дата: ")
    dataAllLabel.grid(row=0, column=0, sticky="w")

    dataAllList = ttk.Combobox(titleEdit, values=days)
    dataAllList.place(x=236, y=3)
    dataAllList.config(width=2)

    dataAllMonthList = ttk.Combobox(titleEdit, values=months)
    dataAllMonthList.place(x=288, y=3)
    dataAllMonthList.config(width=8)

    dataAllYearList = ttk.Combobox(titleEdit, values=years)
    dataAllYearList.place(x=374, y=3)
    dataAllYearList.config(width=2)

    nameSubjectLabel = Label(
        titleEdit, text="Наименование дисциплины (модуля): ")
    nameSubjectLabel.grid(row=1, column=0, sticky="w")

    #ВЫПАДАЮЩИЙ СПИСОК

    obj = Parser()

    subjectsList = ttk.Combobox(titleEdit, values=obj.get_disciplines())
    subjectsList.place(x=236, y=23)
    subjectsList.config(width=60)

    profileLabel = Label(
        titleEdit, text="Направленность (профиль) подготовки: ")
    profileLabel.grid(row=2, column=0, sticky="w")
    profileList = ttk.Combobox(titleEdit, values=obj.get_profiles())
    profileList.config(width=60)
    profileList.place(x=236, y=43)

    protocolLabel = Label(titleEdit, text="Протокол ")
    protocolLabel.grid(row=3, column=0, sticky="w")

    protocolNumLabel = Label(titleEdit, text="№ ")
    protocolNumLabel.place(x=210, y=65)
    protocolNumMessage = StringVar()
    protocolNumEntry = Entry(
        titleEdit, textvariable=protocolNumMessage, width=1)
    protocolNumEntry.place(x=236, y=65)

    protocolDataLabel = Label(titleEdit, text="от ")
    protocolDataLabel.place(x=258, y=65)
    protocolDataList = ttk.Combobox(titleEdit, values=days)
    protocolDataList.place(x=280, y=65)
    protocolDataList.config(width=2)

    protocolDataMonthList = ttk.Combobox(titleEdit, values=months)
    protocolDataMonthList.place(x=330, y=65)
    protocolDataMonthList.config(width=8)

    protocolDataMonthYearList = ttk.Combobox(titleEdit, values=years)
    protocolDataMonthYearList.place(x=415, y=65)
    protocolDataMonthYearList.config(width=2)

    now = datetime.datetime.now()
    dateLabel = Label(titleEdit, text="Год: ")
    dateLabel.place(x=0, y=85)
    dateNow = Label(titleEdit, text=now.year)
    dateNow.place(x=25, y=85)
    Button(titleEdit, text="Сохранить", command=saver).grid(
        row=10, column=1, sticky="n")


def openFirst():
    firstEdit = Toplevel(menu)
    firstEdit.title('I. ЦЕЛИ И ЗАДАЧИ ДИСЦИПЛИНЫ (МОДУЛЯ)')
    firstEdit.geometry('600x400')
    firstEdit.resizable(width=False, height=False)

    frame = Frame(firstEdit)

    targetLabel = Label(firstEdit, text="Цели: ")
    targetLabel.grid(row=0, column=0, sticky="w")
    target = StringVar()
    targetEntry = Entry(firstEdit, textvariable=target, width=80)
    targetEntry.grid(row=0, column=1, padx=5, pady=5)

    tasksLabel = Label(firstEdit, text="Задачи: ")
    tasksLabel.grid(row=1, column=0, sticky="w")

    def taskAdd():
        box.insert(END, tasksEntry.get())
        tasksEntry.delete(0, END)

    def taskDel():
        select = list(box.curselection())
        select.reverse()
        for i in select:
            box.delete(i)

    def taskAndTargetOutput():
        print(target.get())
        print("\n".join(box.get(0, END)))

    box = Listbox(firstEdit, selectmode=EXTENDED, width=80)
    box.grid(row=3, column=1, sticky="w")
    scroll = Scrollbar(firstEdit, command=box.yview)
    scroll.grid(row=3, column=0, sticky="e")
    box.config(yscrollcommand=scroll.set)

    tasksEntry = Entry(firstEdit, width=80)
    tasksEntry.grid(row=1, column=1)
    Button(firstEdit, text="Добавить задачу", command=taskAdd).grid(
        row=8, column=1, sticky="n")
    Button(firstEdit, text="Удалить задачу", command=taskDel).grid(
        row=9, column=1, sticky="n")
    Button(firstEdit, text="Сохранить", command=taskAndTargetOutput).grid(
        row=10, column=1, sticky="n")
    frame.place(relwidth=0.8, relheight=0.7)


def openThird():
    def competencesCheck():
      if len(competenceList.get()) != 0:
        group_1 = tk.LabelFrame(thirdEdit, padx=15, pady=10,
                                text="Выбор компетенции")
        group_1.place(x=10, y=60)

        y = 0
        competenceNameList = []
        for i in competenceList.get():
          if i != " ":
            competenceNameList.append(i)
          else:
            break
        competenceName = ''.join(competenceNameList)
        competenceNameList = []
        competenceFinalList = []
        competenceDescriptionList = []
        for i in competenceSecondList:
          if (i[0].startswith(competenceName) and i[0] != ""):
            competenceFinalList.append(i[0])
            competenceDescriptionList.append(1)

        for j in competenceFinalList:
          y += 1
          Checkbutton(group_1, text=j, onvalue=1, offvalue=0).grid(row=y)

    thirdEdit = Toplevel(menu)
    thirdEdit.title('III. ТРЕБОВАНИЯ К РЕЗУЛЬТАТАМ ОСВОЕНИЯ ДИСЦИПЛИНЫ')
    thirdEdit.geometry('600x400')
    thirdEdit.resizable(width=False, height=False)

    frame = Frame(thirdEdit)

    competenceLabel = Label(thirdEdit, text="Компетиция: ")
    competenceLabel.grid(row=0, column=0, sticky="w")

    competenceBtn = Button(thirdEdit, text="Выбор", command=competencesCheck)
    competenceBtn.place(x=30, y=30)

    obj = Parser()
    comp = list(obj.get_competences().keys())
    competenceList = ttk.Combobox(thirdEdit, values=comp)
    competenceList.place(x=80, y=0)
    competenceSecondList = []
    for i in list(obj.get_competences().values()):
      for j in i:
        competenceSecondList.append(list(j.keys()))


menu = Tk()
menu.title('Выбор темы')
menu.geometry('600x400')
menu.resizable(width=False, height=False)

frame = Frame(menu)

MenuLabel = Label(menu, text="Выбор темы:", font="helvetica 14 bold italic")
MenuLabel.pack()

titleBtn = Button(menu, text="ТИТУЛЬНЫЙ ЛИСТ",
                  command=openTitle, bg="#127545", fg="white")
titleBtn.pack()
firstBtn = Button(menu, text="I. ЦЕЛИ И ЗАДАЧИ ДИСЦИПЛИНЫ (МОДУЛЯ)",
                  command=openFirst, bg="#127545", fg="white")
firstBtn.pack()
secondBtn = Button(menu, text="II. МЕСТО ДИСЦИПЛИНЫ В СТРУКТУРЕ ОПОП ВО",
                   command=openFirst, bg="#127545", fg="white")
secondBtn.pack()
thirdBtn = Button(menu, text="III. ТРЕБОВАНИЯ К РЕЗУЛЬТАТАМ ОСВОЕНИЯ ДИСЦИПЛИНЫ",
                  command=openThird, bg="#127545", fg="white")
thirdBtn.pack()
fourthBtn = Button(menu, text="IV. СОДЕРЖАНИЕ И СТРУКТУРА ДИСЦИПЛИНЫ",
                   command=openFirst, bg="#127545", fg="white")
fourthBtn.pack()
fifthBtn = Button(menu, text="V. УЧЕБНО-МЕТОДИЧЕСКОЕ И ИНФОРМАЦИОННОЕ ОБЕСПЕЧЕНИЕ ДИСЦИПЛИНЫ (МОДУЛЯ)",
                  command=openFirst, bg="#127545", fg="white")
fifthBtn.pack()
frame.place(relwidth=0.8, relheight=0.7)

menu.mainloop()
